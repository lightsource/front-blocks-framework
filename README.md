## The package is deprecated. See [front-blocks](https://gitlab.com/lightsource/front-blocks) instead

## Front blocks framework

[1. Advantages](#advantages)  
[2. How to use](#how-to-use)  
[3. Framework structure](#framework-structure)  
[4. Requirements](#requirements)  
[5. Examples of usage](#examples-of-usage)  
[6. Additional info](#additional-info)

## Advantages

Using this mini framework (including like a composer package) helps create a structure and combine front-end resources (
html/js/css) into reusable blocks (modules). Framework inspired by the BEM methodology and simplify organization and
using front-end blocks (modules).

* Inspired by BEM
* Allows keep all resources grouped, that simplify editing and improving code reading
* Uses MVC way for each block
* Supports dependencies between blocks, that allows use a block in another block and joins resources output (js, css)
  in the right order (in case when using a dynamic build)
* Supports dynamic resource build (like js, css) on a flight
* Uses Twig as a template engine, friendly to scss and webpack (see [Examples of usage](#examples-of-usage))

## How to use

1. Install the composer package

`composer require lightsource/front-blocks-framework`

2. Create settings and block's instances

```
use LightSource\FrontBlocksFramework\{
	Blocks,
	Settings
};

require_once __DIR__ . '/vendors/vendor/autoload.php';

$settings = new Settings();
$settings->setBlocksDirNamespace( 'LightSource\FrontBlocksExample' );
$settings->setBlocksDirPath( __DIR__ . '/Blocks' );
$settings->setErrorCallback( function ( array $errors ) {
	// todo log or any other actions
	echo '<pre>' . print_r( $errors, true ) . '</pre>';
}
);
$blocks = new Blocks( $settings );       
```

3. Create blocks

```
/Blocks
    Homepage
        homepage.twig
        homepage.css
        Homepage.php
        HomepageC.php           
```

4. Load and render blocks

```
$homepageController = new HomepageC();  
$homepageController->getModel()->loadByTest();

$content = $blocks->renderBlock( $homepageController );
$css     = $blocks->getUsedResources( '.css', true );
$js      = $blocks->getUsedResources( '.js', true ); 
```

See [Examples of usage](#examples-of-usage) to get more info.

## Framework structure

### Block

* **Static resources**
    1. twig, scss-css/min.css, js/min.js, png...

* **Model - provides fields for a twig template**
    1. All protected fields will be used as arguments to a twig template.
    2. Can contain standard fields (string, int...) and other Models, if this fields have declared types then they will
       be auto initialized with a default value (include with a Model type)
    4. Can have a parent-children relation (so can be extended)

* **Controller - resources manager, for relate resources between themselves and for relate Model and a template**
    1. Contains a Model field, which will be auto initialized with a related Model instance (by naming convention, see the [Requirements](#requirements))
    2. All protected fields with the Controller type will be:
        - auto initialized with a related instance
        - marked as dependencies of this block
        - used together with relative Model fields (if there are, by name) to get template arguments
    3. Can have a parent-children relation (so can be extended)

### Blocks

* Autoload all Controllers (within a folder, by settings)
* Provide a render function (for a Controller instance)
* Contains a list of used Blocks (with dependencies) in the right order
* Can get used css/js (thanks to the previous list)

## Requirements

- php 7.4+
- All blocks should have one parent folder
- Models and Controllers should have a PSR-4 compatible namespace with an autoloader
- **Naming convention**:
    * Controller name should have a 'C' suffix ('Namespace\Block\BlockC') (the suffix can be changed)
    * Model name should have the same namespace and name (without suffix) as a related controller (
      'Namespace\Block\Block')
    * Resource name should be the same as a controller name, but with those differences :
        - Without the controller suffix
        - CamelCase should be converted to a lower case with a dash ('-') ('BlockName' = 'block-name')
        - Underline ('_') should be replaced with a dash ('-') ('block_theme_main' = 'block-theme-main')
        - an example for above's rules : 'Block_Theme_MainC' should be converted into 'block--theme--main'
- Using the BEM methodology isn't required but highly recommended

## Examples of usage

1. [Example 1](https://github.com/light-source/front-blocks-example) - without scss & webpack
2. [Example 2](https://github.com/light-source/front-blocks-webpack-example) - with scss & webpack

## Additional info

* Twig template:
    - **additional keys** ('_template', '_isLoaded') for each block are available ('_isLoaded' will be true after the _
      load() method call in related Model)
    - **'_merge' filter** is available (merging arrays recursively unlike of the standard merge)
    - **'_include' function** (`blockArgs,additionalArgs`) which uses the additional keys for blocks include is
      available, so you can include blocks like it `{{ _include(blockName,{classes:['block-name',]} )` and it'll locate
      a template by the '_template' field and will render only if '_isLoaded' is set
* Each controller can override the static ::onLoad() method which called once during loading to add own functionality (e.g.
  ajax handlers or something else)
* **Tool for copy blocks** (with names replacing) is available  
  E.g. in the Blocks folder the
  command `{pathToComposer}/vendor/bin/fbf copy Source/SourceC.php Target/TargetC.php C` will copy the Controller and
  all siblings files and will do name replacing, so you can create an example block and reproduce new blocks from it.

   
