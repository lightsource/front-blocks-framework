<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework;

class BlockDuplicator
{

    private string $blocksFolder;
    private string $sourceControllerFile;
    private string $targetControllerFile;
    private array $files;
    private string $controllerSuffix;

    public function __construct(
        string $blocksFolder,
        string $sourceControllerFile,
        string $targetControllerFile,
        string $controllerSuffix
    ) {
        $this->blocksFolder         = $blocksFolder;
        $this->sourceControllerFile = $sourceControllerFile;
        $this->targetControllerFile = $targetControllerFile;
        $this->files                = [];
        $this->controllerSuffix     = $controllerSuffix;
    }

    private function getBlocksPath(string $target): string
    {
        return implode(DIRECTORY_SEPARATOR, [$this->blocksFolder, $target,]);
    }

    private function copyFolderFiles(string $source, string $target): bool
    {
        $isCopied = false;

        if (
            ! is_dir($source) ||
            is_dir($target)
        ) {
            return $isCopied;
        }

        mkdir($target, 0777, true);

        $sourceHandle = opendir($source);

        while (($fileName = readdir($sourceHandle))) {
            if (in_array($fileName, ['.', '..',], true)) {
                continue;
            }

            $sourceFile = $source . DIRECTORY_SEPARATOR . $fileName;
            $targetFile = $target . DIRECTORY_SEPARATOR . $fileName;

            if (is_dir($sourceFile)) {
                continue;
            }

            copy($sourceFile, $targetFile);

            $this->files[] = $targetFile;
        }

        closedir($sourceHandle);


        return true;
    }

    private function getControllerClass(string $controllerFile): string
    {
        $controllerClass = str_replace('/', '\\', $controllerFile);

        return str_replace('.php', '', $controllerClass);
    }

    private function getControllerResourceInfo(string $controllerClass): array
    {
        $settings = new Settings();
        $settings->setControllerSuffix($this->controllerSuffix);

        return Controller::getResourceInfo($settings, $controllerClass);
    }

    private function getControllerClassNameWithoutSuffix(string $controllerClass): string
    {
        $controllerClass = explode('\\', $controllerClass);
        $controllerClass = $controllerClass[count($controllerClass) - 1];

        return substr($controllerClass, 0, mb_strlen($controllerClass) - mb_strlen($this->controllerSuffix));
    }

    private function getControllerNamespace(string $controllerClass): string
    {
        $namespace = explode('\\', $controllerClass);
        $namespace = array_slice($namespace, 0, count($namespace) - 1);

        return implode('\\', $namespace);
    }

    private function replaceNameInFiles(string $originName, string $targetName)
    {
        for ($i = 0; $i < count($this->files); $i++) {
            $fileName    = basename($this->files[$i]);
            $parentPath  = dirname($this->files[$i]);
            $newFileName = preg_replace("/{$originName}/", $targetName, $fileName);

            if ($newFileName === $fileName) {
                continue;
            }

            $pathToNewFile = $parentPath . DIRECTORY_SEPARATOR . $newFileName;

            rename($parentPath . DIRECTORY_SEPARATOR . $fileName, $pathToNewFile);

            $this->files[$i] = $pathToNewFile;
        }
    }

    private function replaceContentInFiles(string $originContent, string $targetContent)
    {
        foreach ($this->files as $pathToFile) {
            $fileContent = file_get_contents($pathToFile);

            $newFileContent = preg_replace("/{$originContent}/", $targetContent, $fileContent);

            if ($newFileContent === $fileContent) {
                continue;
            }

            file_put_contents($pathToFile, $newFileContent);
        }
    }

    private function replaceInResources(string $sourceControllerClass, string $targetControllerClass): void
    {
        $sourceResourceName = $this->getControllerResourceInfo($sourceControllerClass)['resourceName'];
        $targetResourceName = $this->getControllerResourceInfo($targetControllerClass)['resourceName'];

        $this->replaceNameInFiles($sourceResourceName, $targetResourceName);
        $this->replaceContentInFiles($sourceResourceName, $targetResourceName);
    }

    private function replaceInModelAndController(string $sourceControllerClass, string $targetControllerClass): void
    {
        $sourceControllerClassNameWithoutSuffix = $this->getControllerClassNameWithoutSuffix($sourceControllerClass);
        $targetControllerClassNameWithoutSuffix = $this->getControllerClassNameWithoutSuffix($targetControllerClass);

        $sourceControllerNamespace = addslashes($this->getControllerNamespace($sourceControllerClass));
        $targetControllerNamespace = addslashes($this->getControllerNamespace($targetControllerClass));

        $this->replaceNameInFiles($sourceControllerClassNameWithoutSuffix, $targetControllerClassNameWithoutSuffix);
        $this->replaceContentInFiles($sourceControllerClassNameWithoutSuffix, $targetControllerClassNameWithoutSuffix);
        $this->replaceContentInFiles($sourceControllerNamespace, $targetControllerNamespace);
    }

    public function copy(): bool
    {
        $sourceFolder = $this->getBlocksPath(dirname($this->sourceControllerFile));
        $targetFolder = $this->getBlocksPath(dirname($this->targetControllerFile));

        if (! $this::copyFolderFiles($sourceFolder, $targetFolder)) {
            return false;
        }

        $sourceControllerClass = $this->getControllerClass($this->sourceControllerFile);
        $targetControllerClass = $this->getControllerClass($this->targetControllerFile);

        $this->replaceInResources($sourceControllerClass, $targetControllerClass);
        $this->replaceInModelAndController($sourceControllerClass, $targetControllerClass);

        return true;
    }
}
