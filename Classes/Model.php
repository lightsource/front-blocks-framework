<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework;

abstract class Model extends FieldsReader
{

    private bool $isLoaded;

    public function __construct()
    {
        parent::__construct();

        $this->isLoaded = false;
    }

    final public function isLoaded(): bool
    {
        return $this->isLoaded;
    }

    public function getFields(): array
    {
        $args = [];

        $fieldsInfo = $this->getFieldsInfo();

        foreach ($fieldsInfo as $fieldName => $fieldType) {
            $args[$fieldName] = $this->{$fieldName};
        }

        return $args;
    }

    final protected function load(): void
    {
        $this->isLoaded = true;
    }
}
