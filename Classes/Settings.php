<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework;

class Settings
{

    public static string $controllerSuffix = 'C';

    private string $blocksDirPath;
    private string $blocksDirNamespace;
    private array $twigArgs;
    private string $twigExtension;
    private $errorCallback;

    public function __construct()
    {
        $this->blocksDirPath      = '';
        $this->blocksDirNamespace = '';
        $this->twigArgs           = [
            // will generate exception if a var doesn't exist instead of replace to NULL
            'strict_variables' => true,
            // disable autoescape to prevent break data
            'autoescape'       => false,
        ];
        $this->twigExtension      = '.twig';
        $this->errorCallback      = null;
    }

    public function setBlocksDirPath(string $blocksDirPath): void
    {
        $this->blocksDirPath = $blocksDirPath;
    }

    public function setBlocksDirNamespace(string $blocksDirNamespace): void
    {
        $this->blocksDirNamespace = $blocksDirNamespace;
    }

    public function setTwigArgs(array $twigArgs): void
    {
        $this->twigArgs = array_merge($this->twigArgs, $twigArgs);
    }

    public function setErrorCallback(?callable $errorCallback): void
    {
        $this->errorCallback = $errorCallback;
    }

    public function setTwigExtension(string $twigExtension): void
    {
        $this->twigExtension = $twigExtension;
    }

    public function setControllerSuffix(string $controllerSuffix): void
    {
        $this->_controllerSuffix = $controllerSuffix;
    }

    public function getBlocksDirPath(): string
    {
        return $this->blocksDirPath;
    }

    public function getBlocksDirNamespace(): string
    {
        return $this->blocksDirNamespace;
    }

    public function getTwigArgs(): array
    {
        return $this->twigArgs;
    }

    public function getTwigExtension(): string
    {
        return $this->twigExtension;
    }

    public function callErrorCallback(array $errors): void
    {
        if (! is_callable($this->errorCallback)) {
            return;
        }

        call_user_func_array($this->errorCallback, [$errors,]);
    }
}
