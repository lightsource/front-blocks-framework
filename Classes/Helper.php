<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework;

abstract class Helper
{

    final public static function arrayFilter(array $array, callable $callback, bool $isSaveKeys): array
    {
        $arrayResult = array_filter($array, $callback);

        return $isSaveKeys ?
            $arrayResult :
            array_values($arrayResult);
    }

    final public static function arrayMergeRecursive(array $args1, array $args2): array
    {
        foreach ($args2 as $key => $value) {
            if (intval($key) === $key) {
                $args1[] = $value;

                continue;
            }

            // recursive sub-merge for internal arrays
            if (
                is_array($value) &&
                key_exists($key, $args1) &&
                is_array($args1[$key])
            ) {
                $value = self::arrayMergeRecursive($args1[$key], $value);
            }

            $args1[$key] = $value;
        }

        return $args1;
    }
}
