<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework;

class Blocks
{

    private array $loadedControllerClasses;
    private array $usedControllerClasses;
    private Settings $settings;
    private Twig $twig;

    public function __construct(Settings $settings)
    {
        $this->loadedControllerClasses = [];
        $this->usedControllerClasses   = [];
        $this->settings                = $settings;
        $this->twig                    = new Twig($settings);
    }

    final public function getLoadedControllerClasses(): array
    {
        return $this->loadedControllerClasses;
    }

    final public function getUsedControllerClasses(): array
    {
        return $this->usedControllerClasses;
    }

    final public function getSettings(): Settings
    {
        return $this->settings;
    }

    final public function getTwig(): Twig
    {
        return $this->twig;
    }

    final public function getUsedResources(string $extension, bool $isIncludeSource = false): string
    {
        $resourcesContent = '';

        foreach ($this->usedControllerClasses as $usedControllerClass) {
            $getResourcesInfoCallback = [$usedControllerClass, 'getResourceInfo'];

            if (! is_callable($getResourcesInfoCallback)) {
                $this->settings->callErrorCallback(
                    [
                        'message' => "Controller class doesn't exist",
                        'class'   => $usedControllerClass,
                    ]
                );

                continue;
            }

            $resourceInfo = call_user_func_array(
                $getResourcesInfoCallback,
                [
                    $this->settings,
                ]
            );

            $pathToResourceFile = $this->settings->getBlocksDirPath() .
                                  DIRECTORY_SEPARATOR . $resourceInfo['relativeResourcePath'] . $extension;

            if (! is_file($pathToResourceFile)) {
                continue;
            }

            $resourcesContent .= $isIncludeSource ?
                "\n/* " . $resourceInfo['resourceName'] . " */\n" :
                '';

            $resourcesContent .= file_get_contents($pathToResourceFile);
        }

        return $resourcesContent;
    }

    private function loadController(string $phpClass, array $debugArgs): bool
    {
        $isLoaded = false;

        if (
            ! class_exists($phpClass, true) ||
            ! is_subclass_of($phpClass, Controller::class)
        ) {
            $this->settings->callErrorCallback(
                [
                    'message' => "Class doesn't exist or doesn't child",
                    'args'    => $debugArgs,
                ]
            );

            return $isLoaded;
        }

        call_user_func([$phpClass, 'onLoad']);

        return true;
    }

    private function loadControllers(string $directory, string $namespace, array $controllerFileNames): void
    {
        foreach ($controllerFileNames as $controllerFileName) {
            $phpFile   = implode(DIRECTORY_SEPARATOR, [$directory, $controllerFileName]);
            $phpClass  = implode('\\', [$namespace, str_replace('.php', '', $controllerFileName),]);
            $debugArgs = [
                'directory' => $directory,
                'namespace' => $namespace,
                'phpFile'   => $phpFile,
                'phpClass'  => $phpClass,
            ];

            if (! $this->loadController($phpClass, $debugArgs)) {
                continue;
            }

            $this->loadedControllerClasses[] = $phpClass;
        }
    }

    private function loadDirectory(string $directory, string $namespace): void
    {
        // exclude ., ..
        $fs = array_diff(scandir($directory), ['.', '..']);

        $controllerFilePreg = '/' . Settings::$controllerSuffix . '.php$/';

        $controllerFileNames = Helper::arrayFilter(
            $fs,
            function ($f) use ($controllerFilePreg) {
                return (1 === preg_match($controllerFilePreg, $f));
            },
            false
        );
        $subDirectoryNames   = Helper::arrayFilter(
            $fs,
            function ($f) {
                return false === strpos($f, '.');
            },
            false
        );

        foreach ($subDirectoryNames as $subDirectoryName) {
            $subDirectory = implode(DIRECTORY_SEPARATOR, [$directory, $subDirectoryName]);
            $subNamespace = implode('\\', [$namespace, $subDirectoryName]);

            $this->loadDirectory($subDirectory, $subNamespace);
        }

        $this->loadControllers($directory, $namespace, $controllerFileNames);
    }

    final public function loadAll(): void
    {
        $directory = $this->settings->getBlocksDirPath();
        $namespace = $this->settings->getBlocksDirNamespace();

        $this->loadDirectory($directory, $namespace);
    }

    final public function renderBlock(Controller $controller, array $args = [], bool $isPrint = false): string
    {
        $dependencies                = array_merge($controller->getDependencies(), [get_class($controller),]);
        $newDependencies             = array_diff($dependencies, $this->usedControllerClasses);
        $this->usedControllerClasses = array_merge($this->usedControllerClasses, $newDependencies);

        $templateArgs = $controller->getTemplateArgs($this->settings);
        $templateArgs = Helper::arrayMergeRecursive($templateArgs, $args);

        return $this->twig->render($templateArgs[Controller::TEMPLATE_KEY__TEMPLATE], $templateArgs, $isPrint);
    }
}
