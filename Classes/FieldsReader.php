<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework;

use Exception;
use ReflectionProperty;

abstract class FieldsReader
{

    private array $fieldsInfo;

    public function __construct()
    {
        $this->fieldsInfo = [];

        $this->readFieldsInfo();
        $this->autoInitFields();
    }

    final protected function getFieldsInfo(): array
    {
        return $this->fieldsInfo;
    }

    protected function getFieldType(string $fieldName): ?string
    {
        $fieldType = null;

        try {
            // used static for child support
            $property = new ReflectionProperty(static::class, $fieldName);
        } catch (Exception $ex) {
            return $fieldType;
        }

        if (! $property->isProtected()) {
            return $fieldType;
        }

        return $property->getType() ?
            $property->getType()->getName() :
            '';
    }

    private function readFieldsInfo(): void
    {
        $fieldNames = array_keys(get_class_vars(static::class));

        foreach ($fieldNames as $fieldName) {
            $fieldType = $this->getFieldType($fieldName);

            // only protected fields
            if (is_null($fieldType)) {
                continue;
            }

            $this->fieldsInfo[$fieldName] = $fieldType;
        }
    }

    private function autoInitFields(): void
    {
        foreach ($this->fieldsInfo as $fieldName => $fieldType) {
            // ignore fields without a type
            if (! $fieldType) {
                continue;
            }

            $defaultValue = null;

            switch ($fieldType) {
                case 'int':
                case 'float':
                    $defaultValue = 0;
                    break;
                case 'bool':
                    $defaultValue = false;
                    break;
                case 'string':
                    $defaultValue = '';
                    break;
                case 'array':
                    $defaultValue = [];
                    break;
            }

            try {
                if (
                    is_subclass_of($fieldType, Model::class) ||
                    is_subclass_of($fieldType, Controller::class)
                ) {
                    $defaultValue = new $fieldType();
                }
            } catch (Exception $ex) {
                $defaultValue = null;
            }

            // ignore fields with a custom type (null by default)
            if (is_null($defaultValue)) {
                continue;
            }

            $this->{$fieldName} = $defaultValue;
        }
    }
}
