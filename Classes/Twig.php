<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework;

use Exception;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\Loader\LoaderInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

class Twig
{

    private ?LoaderInterface $twigLoader;
    private ?Environment $twigEnvironment;
    private Settings $settings;

    public function __construct(Settings $settings, ?LoaderInterface $twigLoader = null)
    {
        $this->twigEnvironment = null;
        $this->settings        = $settings;
        $this->twigLoader      = $twigLoader;

        $this->init();
    }

    // e.g for extend a twig with adding a new filter
    public function getEnvironment(): ?Environment
    {
        return $this->twigEnvironment;
    }

    private function extendTwig(): void
    {
        $this->twigEnvironment->addFilter(
            new TwigFilter(
                '_merge',
                function ($source, $additional) {
                    return Helper::arrayMergeRecursive($source, $additional);
                }
            )
        );
        $this->twigEnvironment->addFunction(
            new TwigFunction(
                '_include',
                function ($block, $args = []) {
                    $block = Helper::arrayMergeRecursive($block, $args);

                    return $block[Controller::TEMPLATE_KEY__IS_LOADED] ?
                        $this->render($block[Controller::TEMPLATE_KEY__TEMPLATE], $block) :
                        '';
                }
            )
        );
    }

    private function init(): void
    {
        try {
            $this->twigLoader      = ! $this->twigLoader ?
                new FilesystemLoader($this->settings->getBlocksDirPath()) :
                $this->twigLoader;
            $this->twigEnvironment = new Environment($this->twigLoader, $this->settings->getTwigArgs());
        } catch (Exception $ex) {
            $this->twigEnvironment = null;

            $this->settings->callErrorCallback(
                [
                    'message' => $ex->getMessage(),
                    'file'    => $ex->getFile(),
                    'line'    => $ex->getLine(),
                    'trace'   => $ex->getTraceAsString(),
                ]
            );

            return;
        }

        $this->extendTwig();
    }

    public function render(string $template, array $args = [], bool $isPrint = false): string
    {
        $html = '';

        // twig isn't loaded
        if (is_null($this->twigEnvironment)) {
            return $html;
        }

        try {
            // will generate ean exception if a template doesn't exist OR broken
            // also if a var doesn't exist (if using a 'strict_variables' flag, see Twig_Environment->__construct)
            $html .= $this->twigEnvironment->render($template, $args);
        } catch (Exception $ex) {
            $html = '';

            $this->settings->callErrorCallback(
                [
                    'message'  => $ex->getMessage(),
                    'file'     => $ex->getFile(),
                    'line'     => $ex->getLine(),
                    'trace'    => $ex->getTraceAsString(),
                    'template' => $template,
                ]
            );
        }

        if ($isPrint) {
            echo $html;
        }

        return $html;
    }
}
