<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework;

use Exception;

abstract class Controller extends FieldsReader
{

    public const TEMPLATE_KEY__TEMPLATE = '_template';
    public const TEMPLATE_KEY__IS_LOADED = '_isLoaded';

    private ?Model $model;
    private array $external;

    public function __construct(?Model $model = null)
    {
        parent::__construct();

        $this->model    = $model;
        $this->external = [];

        $this->autoInitModel();
    }

    final public static function getResourceInfo(Settings $settings, string $controllerClass = ''): array
    {
        // using static for children support
        $controllerClass = ! $controllerClass ?
            static::class :
            $controllerClass;

        // e.g. $controllerClass = Example/Theme/Main/Example_Theme_Main_C
        $resourceInfo = [
            'resourceName'         => '',// e.g. example--theme--main
            'relativePath'         => '',// e.g. Example/Theme/Main
            'relativeResourcePath' => '', // e.g. Example/Theme/Main/example--theme--main
        ];

        $controllerSuffix = Settings::$controllerSuffix;

        //  e.g. Example/Theme/Main/Example_Theme_Main
        $relativeControllerNamespace = $settings->getBlocksDirNamespace() ?
            str_replace($settings->getBlocksDirNamespace() . '\\', '', $controllerClass) :
            $controllerClass;
        $relativeControllerNamespace = substr(
            $relativeControllerNamespace,
            0,
            mb_strlen($relativeControllerNamespace) - mb_strlen($controllerSuffix)
        );

        // e.g. Example_Theme_Main
        $phpBlockName = explode('\\', $relativeControllerNamespace);
        $phpBlockName = $phpBlockName[count($phpBlockName) - 1];

        // e.g. example--theme--main (from Example_Theme_Main)
        $blockNameParts    = preg_split('/(?=[A-Z])/', $phpBlockName, -1, PREG_SPLIT_NO_EMPTY);
        $blockResourceName = [];
        foreach ($blockNameParts as $blockNamePart) {
            $blockResourceName[] = strtolower($blockNamePart);
        }
        $blockResourceName = implode('-', $blockResourceName);
        $blockResourceName = str_replace('_', '-', $blockResourceName);

        // e.g. Example/Theme/Main
        $relativePath = explode('\\', $relativeControllerNamespace);
        $relativePath = array_slice($relativePath, 0, count($relativePath) - 1);
        $relativePath = implode(DIRECTORY_SEPARATOR, $relativePath);

        $resourceInfo['resourceName']         = $blockResourceName;
        $resourceInfo['relativePath']         = $relativePath;
        $resourceInfo['relativeResourcePath'] = $relativePath . DIRECTORY_SEPARATOR . $blockResourceName;

        return $resourceInfo;
    }

    // can be overridden if Controller doesn't have own twig (uses parents)
    public static function getPathToTwigTemplate(Settings $settings, string $controllerClass = ''): string
    {
        return self::getResourceInfo($settings, $controllerClass)['relativeResourcePath'] .
               $settings->getTwigExtension();
    }

    // can be overridden if Controller doesn't have own model (uses parents)
    public static function getModelClass(): string
    {
        $controllerClass = static::class;
        $modelClass      = rtrim($controllerClass, Settings::$controllerSuffix);

        return ($modelClass !== $controllerClass &&
                class_exists($modelClass, true) &&
                is_subclass_of($modelClass, Model::class) ?
            $modelClass :
            '');
    }

    public static function onLoad()
    {
    }

    final public function setModel(Model $model): void
    {
        $this->model = $model;
    }

    final protected function setExternal(string $fieldName, array $args): void
    {
        $this->external[$fieldName] = $args;
    }

    private function getControllerField(string $fieldName): ?Controller
    {
        $controller = null;
        $fieldsInfo = $this->getFieldsInfo();

        if (key_exists($fieldName, $fieldsInfo)) {
            $controller = $this->{$fieldName};

            // prevent possible recursion by a mistake (if someone will create a field with self)
            // using static for children support
            $controller = ($controller &&
                           $controller instanceof Controller &&
                           get_class($controller) !== static::class) ?
                $controller :
                null;
        }

        return $controller;
    }

    public function getTemplateArgs(Settings $settings): array
    {
        $modelFields  = $this->model ?
            $this->model->getFields() :
            [];
        $templateArgs = [];

        foreach ($modelFields as $modelFieldName => $modelFieldValue) {
            if (! $modelFieldValue instanceof Model) {
                $templateArgs[$modelFieldName] = $modelFieldValue;
                continue;
            }

            $modelFieldController = $this->getControllerField($modelFieldName);
            $modelFieldArgs       = [];
            $externalFieldArgs    = $this->external[$modelFieldName] ?? [];

            if ($modelFieldController) {
                $modelFieldController->setModel($modelFieldValue);
                $modelFieldArgs = $modelFieldController->getTemplateArgs($settings);
            }

            $templateArgs[$modelFieldName] = Helper::arrayMergeRecursive($modelFieldArgs, $externalFieldArgs);
        }

        // using static for children support
        return array_merge(
            $templateArgs,
            [
                self::TEMPLATE_KEY__TEMPLATE  => static::getPathToTwigTemplate($settings),
                self::TEMPLATE_KEY__IS_LOADED => ($this->model && $this->model->isLoaded()),
            ]
        );
    }

    public function getDependencies(string $sourceClass = ''): array
    {
        $dependencyClasses = [];
        $controllerFields  = $this->getFieldsInfo();

        foreach ($controllerFields as $fieldName => $fieldType) {
            $dependencyController = $this->getControllerField($fieldName);

            if (! $dependencyController) {
                continue;
            }

            $dependencyClass = get_class($dependencyController);

            // 1. prevent the possible permanent recursion
            // 2. add only unique elements, because several fields can have the same type
            if (
                ($sourceClass && $dependencyClass === $sourceClass) ||
                in_array($dependencyClass, $dependencyClasses, true)
            ) {
                continue;
            }

            // used static for child support
            $subDependencies = $dependencyController->getDependencies(static::class);
            // only unique elements
            $subDependencies = array_diff($subDependencies, $dependencyClasses);

            // sub dependencies are before the main dependency
            $dependencyClasses = array_merge($dependencyClasses, $subDependencies, [$dependencyClass,]);
        }

        return $dependencyClasses;
    }

    // Can be overridden for declare a target model class and provide an IDE support
    public function getModel(): ?Model
    {
        return $this->model;
    }

    private function autoInitModel()
    {
        if ($this->model) {
            return;
        }

        $modelClass = static::getModelClass();

        try {
            $this->model = $modelClass ?
                new $modelClass() :
                $this->model;
        } catch (Exception $ex) {
            $this->model = null;
        }
    }
}
