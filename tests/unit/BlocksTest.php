<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework\Tests\unit;

use Codeception\Test\Unit;
use Exception;
use LightSource\FrontBlocksFramework\Blocks;
use LightSource\FrontBlocksFramework\Controller;
use LightSource\FrontBlocksFramework\Model;
use LightSource\FrontBlocksFramework\Settings;
use LightSource\FrontBlocksFramework\Twig;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;

class BlocksTest extends Unit
{

    private function getBlocks(
        string $namespace,
        vfsStreamDirectory $rootDirectory,
        array $structure,
        array $usedControllerClasses = []
    ): ?Blocks {
        vfsStream::create($structure, $rootDirectory);

        $settings = new Settings();
        $settings->setBlocksDirNamespace($namespace);
        $settings->setBlocksDirPath($rootDirectory->url());

        $twig = $this->make(
            Twig::class,
            [
                'render' => function (string $template, array $args = [], bool $isPrint = false): string {
                    return '';
                },
            ]
        );

        try {
            $blocks = $this->make(
                Blocks::class,
                [
                    'loadedControllerClasses' => [],
                    'usedControllerClasses'   => $usedControllerClasses,
                    'twig'                    => $twig,
                    'settings'                => $settings,
                ]
            );
        } catch (Exception $ex) {
            $this->fail("Can't make Blocks stub, " . $ex->getMessage());
        }

        $blocks->loadAll();

        return $blocks;
    }

    // get a unique namespace depending on a test method to prevent affect other tests
    private function getUniqueControllerNamespaceWithAutoloader(
        string $methodConstant,
        vfsStreamDirectory $rootDirectory
    ): string {
        $namespace = str_replace('::', '_', $methodConstant);

        spl_autoload_register(
            function ($class) use ($rootDirectory, $namespace) {
                $targetNamespace = $namespace . '\\';
                if (0 !== strpos($class, $targetNamespace)) {
                    return;
                }

                $relativePathToFile = str_replace($targetNamespace, '', $class);
                $relativePathToFile = str_replace('\\', '/', $relativePathToFile);

                $absPathToFile = $rootDirectory->url() . DIRECTORY_SEPARATOR . $relativePathToFile . '.php';

                include_once $absPathToFile;
            }
        );

        return $namespace;
    }

    // get a unique directory name depending on a test method to prevent affect other tests
    private function getUniqueDirectory(string $methodConstant): vfsStreamDirectory
    {
        $dirName = str_replace([':', '\\'], '_', $methodConstant);

        return vfsStream::setup($dirName);
    }

    private function getControllerClassFile(string $namespace, string $class): string
    {
        $vendorControllerClass = '\LightSource\FrontBlocksFramework\Controller';

        return '<?php namespace ' . $namespace . '; class ' . $class . ' extends ' . $vendorControllerClass . ' {}';
    }

    private function getController(array $dependencies = [])
    {
        return new class (null, $dependencies) extends Controller {

            private array $dependencies;

            public function __construct(?Model $model = null, array $dependencies)
            {
                parent::__construct($model);
                $this->dependencies = $dependencies;
            }

            public function getDependencies(string $sourceClass = ''): array
            {
                return $this->dependencies;
            }

            public function getTemplateArgs(Settings $settings): array
            {
                return [
                    Controller::TEMPLATE_KEY__TEMPLATE => '',
                ];
            }
        };
    }

    ////

    public function testLoadAllControllersWithPrefix()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks(
            $namespace,
            $rootDirectory,
            [
                'Block' => [
                    'BlockC.php' => $this->getControllerClassFile("{$namespace}\Block", 'BlockC'),
                ],
            ]
        );

        $this->assertEquals(
            [
                "{$namespace}\Block\BlockC",
            ],
            $blocks->getLoadedControllerClasses()
        );
    }

    public function testLoadAllIgnoreControllersWithoutPrefix()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks(
            $namespace,
            $rootDirectory,
            [
                'Block' => [
                    'Block.php' => $this->getControllerClassFile("{$namespace}\Block", 'Block'),
                ],
            ]
        );

        $this->assertEquals([], $blocks->getLoadedControllerClasses());
    }

    public function testLoadAllIgnoreWrongControllers()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks(
            $namespace,
            $rootDirectory,
            [
                'Block' => [
                    'BlockC.php' => $this->getControllerClassFile("{$namespace}\Block", 'WrongBlockC'),
                ],
            ]
        );

        $this->assertEquals([], $blocks->getLoadedControllerClasses());
    }

    ////

    public function testRenderBlockAddsControllerToUsedList()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks($namespace, $rootDirectory, []);
        $controller    = $this->getController();

        $blocks->renderBlock($controller);

        $this->assertEquals(
            [
                get_class($controller),
            ],
            $blocks->getUsedControllerClasses()
        );
    }

    public function testRenderBlockAddsControllerDependenciesToUsedList()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks($namespace, $rootDirectory, []);
        $controller    = $this->getController(['A',]);

        $blocks->renderBlock($controller);

        $this->assertEquals(
            [
                'A',
                get_class($controller),
            ],
            $blocks->getUsedControllerClasses()
        );
    }

    public function testRenderBlockAddsDependenciesBeforeControllerToUsedList()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks($namespace, $rootDirectory, []);
        $controller    = $this->getController(['A',]);

        $blocks->renderBlock($controller);

        $this->assertEquals(
            [
                'A',
                get_class($controller),
            ],
            $blocks->getUsedControllerClasses()
        );
    }

    public function testRenderBlockIgnoreDuplicateControllerWhenAddsToUsedList()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks($namespace, $rootDirectory, []);
        $controllerA   = $this->getController();

        $blocks->renderBlock($controllerA);
        $blocks->renderBlock($controllerA);

        $this->assertEquals(
            [
                get_class($controllerA),
            ],
            $blocks->getUsedControllerClasses()
        );
    }

    public function testRenderBlockIgnoreDuplicateControllerDependenciesWhenAddsToUsedList()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks($namespace, $rootDirectory, []);
        $controllerA   = $this->getController(['A',]);
        $controllerB   = $this->getController(['A',]);

        $blocks->renderBlock($controllerA);
        $blocks->renderBlock($controllerB);

        $this->assertEquals(
            [
                'A',
                get_class($controllerA),// $controllerB has the same class
            ],
            $blocks->getUsedControllerClasses()
        );
    }

    ////

    public function testGetUsedResourcesWhenBlockWithResources()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks(
            $namespace,
            $rootDirectory,
            [
                'Block' => [
                    'BlockC.php' => $this->getControllerClassFile("{$namespace}\Block", 'BlockC'),
                    'block.css'   => 'just css code',
                ],
            ],
            [
                "{$namespace}\Block\BlockC",
            ]
        );

        $this->assertEquals(
            'just css code',
            $blocks->getUsedResources('.css', false)
        );
    }

    public function testGetUsedResourcesWhenBlockWithoutResources()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks(
            $namespace,
            $rootDirectory,
            [
                'Block' => [
                    'BlockC.php' => $this->getControllerClassFile("{$namespace}\Block", 'BlockC'),
                ],
            ],
            [
                "{$namespace}\Block\BlockC",
            ]
        );

        $this->assertEquals(
            '',
            $blocks->getUsedResources('.css', false)
        );
    }

    public function testGetUsedResourcesWhenSeveralBlocks()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks(
            $namespace,
            $rootDirectory,
            [
                'BlockA' => [
                    'BlockAC.php' => $this->getControllerClassFile("{$namespace}\BlockA", 'BlockAC'),
                    'block-a.css'  => 'css code for a',
                ],
                'BlockB' => [
                    'BlockBC.php' => $this->getControllerClassFile("{$namespace}\BlockB", 'BlockBC'),
                    'block-b.css'  => 'css code for b',
                ],
            ],
            [
                "{$namespace}\BlockA\BlockAC",
                "{$namespace}\BlockB\BlockBC",
            ]
        );

        $this->assertEquals(
            'css code for acss code for b',
            $blocks->getUsedResources('.css', false)
        );
    }

    public function testGetUsedResourcesWithIncludedSource()
    {
        $rootDirectory = $this->getUniqueDirectory(__METHOD__);
        $namespace     = $this->getUniqueControllerNamespaceWithAutoloader(__METHOD__, $rootDirectory);
        $blocks        = $this->getBlocks(
            $namespace,
            $rootDirectory,
            [
                'SimpleBlock' => [
                    'SimpleBlockC.php' => $this->getControllerClassFile("{$namespace}\SimpleBlock", 'SimpleBlockC'),
                    'simple-block.css'  => 'css code',
                ],
            ],
            [
                "{$namespace}\SimpleBlock\SimpleBlockC",
            ]
        );

        $this->assertEquals(
            "\n/* simple-block */\ncss code",
            $blocks->getUsedResources('.css', true)
        );
    }
}
