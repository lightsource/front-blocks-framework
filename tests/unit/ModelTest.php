<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework\Tests\unit;

use Codeception\Test\Unit;
use LightSource\FrontBlocksFramework\Model;

class ModelTest extends Unit
{

    public function testGetFields()
    {
        $model = new class extends Model {

            protected string $field1;

            public function __construct()
            {
                parent::__construct();
            }

            public function update()
            {
                $this->field1 = 'just string';
            }
        };

        $model->update();

        $this->assertEquals(
            [
                'field1' => 'just string',
            ],
            $model->getFields()
        );
    }
}
