<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework\Tests\unit;

use Codeception\Test\Unit;
use Exception;
use LightSource\FrontBlocksFramework\Controller;
use LightSource\FrontBlocksFramework\Settings;
use LightSource\FrontBlocksFramework\Twig;
use Twig\Loader\ArrayLoader;

class TwigTest extends Unit
{

    private function renderBlock(array $blocks, string $renderBlock, array $renderArgs = []): string
    {
        $twigLoader = new ArrayLoader($blocks);
        $settings   = new Settings();

        $twig    = new Twig($settings, $twigLoader);
        $content = '';

        try {
            $content = $twig->render($renderBlock, $renderArgs);
        } catch (Exception $ex) {
            $this->fail('Twig render exception, ' . $ex->getMessage());
        }

        return $content;
    }

    public function testExtendTwigIncludeFunctionWhenBlockIsLoaded()
    {
        $blocks      = [
            'block-a.twig' => '{{ _include(blockB) }}',
            'block-b.twig' => 'block-b content',
        ];
        $renderBlock = 'block-a.twig';
        $renderArgs  = [
            'blockB' => [
                Controller::TEMPLATE_KEY__TEMPLATE  => 'block-b.twig',
                Controller::TEMPLATE_KEY__IS_LOADED => true,
            ],
        ];

        $this->assertEquals('block-b content', $this->renderBlock($blocks, $renderBlock, $renderArgs));
    }

    public function testExtendTwigIncludeFunctionWhenBlockNotLoaded()
    {
        $blocks      = [
            'block-a.twig' => '{{ _include(blockB) }}',
            'block-b.twig' => 'block-b content',
        ];
        $renderBlock = 'block-a.twig';
        $renderArgs  = [
            'blockB' => [
                Controller::TEMPLATE_KEY__TEMPLATE  => 'block-b.twig',
                Controller::TEMPLATE_KEY__IS_LOADED => false,
            ],
        ];

        $this->assertEquals('', $this->renderBlock($blocks, $renderBlock, $renderArgs));
    }

    public function testExtendTwigIncludeFunctionWhenArgsPassed()
    {
        $blocks      = [
            'block-a.twig' => '{{ _include(blockB, {classes:["test-class",],}) }}',
            'block-b.twig' => '{{ classes|join(" ") }}',
        ];
        $renderBlock = 'block-a.twig';
        $renderArgs  = [
            'blockB' => [
                Controller::TEMPLATE_KEY__TEMPLATE  => 'block-b.twig',
                Controller::TEMPLATE_KEY__IS_LOADED => true,
                'classes'                           => ['own-class',],
            ],
        ];

        $this->assertEquals('own-class test-class', $this->renderBlock($blocks, $renderBlock, $renderArgs));
    }

    public function testExtendTwigMergeFilter()
    {
        $blocks      = [
            'block-a.twig' => '{{ {"array":["a",],}|_merge({"array":["b",],}).array|join(" ") }}',
        ];
        $renderBlock = 'block-a.twig';
        $renderArgs  = [];

        $this->assertEquals('a b', $this->renderBlock($blocks, $renderBlock, $renderArgs));
    }
}
