<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework\Tests\unit;

use Codeception\Test\Unit;
use LightSource\FrontBlocksFramework\{
    Controller,
    Model,
    Settings
};

class ControllerTest extends Unit
{

    private function getModel(array $fields, bool $isLoaded = false): Model
    {
        return new class ($fields, $isLoaded) extends Model {

            private array $fields;

            public function __construct(array $fields, bool $isLoaded)
            {
                parent::__construct();

                $this->fields = $fields;

                if ($isLoaded) {
                    $this->load();
                }
            }

            public function getFields(): array
            {
                return $this->fields;
            }
        };
    }

    private function getController(?Model $model): Controller
    {
        return new class ($model) extends Controller {

            public function __construct(?Model $model = null)
            {
                parent::__construct($model);
            }
        };
    }

    private function getTemplateArgsWithoutAdditional(array $templateArgs): array
    {
        $templateArgs = array_diff_key(
            $templateArgs,
            [
                Controller::TEMPLATE_KEY__TEMPLATE  => '',
                Controller::TEMPLATE_KEY__IS_LOADED => '',
            ]
        );
        foreach ($templateArgs as $templateKey => $templateValue) {
            if (! is_array($templateValue)) {
                continue;
            }

            $templateArgs[$templateKey] = $this->getTemplateArgsWithoutAdditional($templateValue);
        }

        return $templateArgs;
    }

    ////

    public function testGetResourceInfoWithoutCamelCaseInBlockName()
    {
        $settings = new Settings();
        $settings->setControllerSuffix('C');
        $settings->setBlocksDirNamespace('Namespace');

        $this->assertEquals(
            [
                'resourceName'         => 'block',
                'relativePath'         => 'Block',
                'relativeResourcePath' => 'Block/block',
            ],
            Controller::GetResourceInfo($settings, 'Namespace\\Block\\BlockC')
        );
    }

    public function testGetResourceInfoWithCamelCaseInBlockName()
    {
        $settings = new Settings();
        $settings->setControllerSuffix('C');
        $settings->setBlocksDirNamespace('Namespace');

        $this->assertEquals(
            [
                'resourceName'         => 'block-name',
                'relativePath'         => 'BlockName',
                'relativeResourcePath' => 'BlockName/block-name',
            ],
            Controller::GetResourceInfo($settings, 'Namespace\\BlockName\\BlockNameC')
        );
    }

    public function testGetResourceInfoWithoutCamelCaseInTheme()
    {
        $settings = new Settings();
        $settings->setControllerSuffix('C');
        $settings->setBlocksDirNamespace('Namespace');

        $this->assertEquals(
            [
                'resourceName'         => 'block--theme--main',
                'relativePath'         => 'Block/Theme/Main',
                'relativeResourcePath' => 'Block/Theme/Main/block--theme--main',
            ],
            Controller::GetResourceInfo($settings, 'Namespace\\Block\\Theme\\Main\\Block_Theme_MainC')
        );
    }

    public function testGetResourceInfoWithCamelCaseInTheme()
    {
        $settings = new Settings();
        $settings->setControllerSuffix('C');
        $settings->setBlocksDirNamespace('Namespace');

        $this->assertEquals(
            [
                'resourceName'         => 'block--theme--just-main',
                'relativePath'         => 'Block/Theme/JustMain',
                'relativeResourcePath' => 'Block/Theme/JustMain/block--theme--just-main',
            ],
            Controller::GetResourceInfo($settings, 'Namespace\\Block\\Theme\\JustMain\\Block_Theme_JustMainC')
        );
    }

    ////

    public function testGetTemplateArgsWhenModelContainsBuiltInTypes()
    {
        $settings   = new Settings();
        $model      = $this->getModel(
            [
                'stringVariable' => 'just string',
            ]
        );
        $controller = $this->getController($model);

        $this->assertEquals(
            [
                'stringVariable' => 'just string',
            ],
            $this->getTemplateArgsWithoutAdditional($controller->getTemplateArgs($settings))
        );
    }

    public function testGetTemplateArgsWhenModelContainsAnotherModel()
    {
        $settings = new Settings();

        $modelA              = $this->getModel(
            [
                'modelA' => 'just string from model a',
            ]
        );
        $modelB              = $this->getModel(
            [
                'modelA' => $modelA,
                'modelB' => 'just string from model b',
            ]
        );
        $controllerForModelA = $this->getController(null);
        $controllerForModelB = new class ($modelB, $controllerForModelA) extends Controller {

            protected $modelA;

            public function __construct(?Model $model = null, $controllerForModelA)
            {
                parent::__construct($model);

                $this->modelA = $controllerForModelA;
            }
        };

        $this->assertEquals(
            [
                'modelA' => [
                    'modelA' => 'just string from model a',
                ],
                'modelB' => 'just string from model b',
            ],
            $this->getTemplateArgsWithoutAdditional($controllerForModelB->getTemplateArgs($settings))
        );
    }

    public function testGetTemplateArgsWhenControllerContainsExternalArgs()
    {
        $settings = new Settings();

        $modelA              = $this->getModel(
            [
                'additionalField' => '',
                'modelA'          => 'just string from model a',
            ]
        );
        $modelB              = $this->getModel(
            [
                'modelA' => $modelA,
                'modelB' => 'just string from model b',
            ]
        );
        $controllerForModelA = $this->getController(null);
        $controllerForModelB = new class ($modelB, $controllerForModelA) extends Controller {

            protected $modelA;

            public function __construct(?Model $model = null, $controllerForModelA)
            {
                parent::__construct($model);

                $this->modelA = $controllerForModelA;
                $this->setExternal(
                    'modelA',
                    [
                        'additionalField' => 'additionalValue',
                    ]
                );
            }
        };

        $this->assertEquals(
            [
                'modelA' => [
                    'additionalField' => 'additionalValue',
                    'modelA'          => 'just string from model a',
                ],
                'modelB' => 'just string from model b',
            ],
            $this->getTemplateArgsWithoutAdditional($controllerForModelB->getTemplateArgs($settings))
        );
    }

    public function testGetTemplateArgsContainsAdditionalFields()
    {
        $settings   = new Settings();
        $model      = $this->getModel([]);
        $controller = $this->getController($model);

        $this->assertEquals(
            [
                Controller::TEMPLATE_KEY__TEMPLATE,
                Controller::TEMPLATE_KEY__IS_LOADED,
            ],
            array_keys($controller->getTemplateArgs($settings))
        );
    }

    public function testGetTemplateArgsWhenAdditionalIsLoadedIsFalse()
    {
        $settings   = new Settings();
        $model      = $this->getModel([]);
        $controller = $this->getController($model);

        $actual = array_intersect_key(
            $controller->getTemplateArgs($settings),
            [Controller::TEMPLATE_KEY__IS_LOADED => '',]
        );

        $this->assertEquals([Controller::TEMPLATE_KEY__IS_LOADED => false,], $actual);
    }

    public function testGetTemplateArgsWhenAdditionalIsLoadedIsTrue()
    {
        $settings   = new Settings();
        $model      = $this->getModel([], true);
        $controller = $this->getController($model);

        $actual = array_intersect_key(
            $controller->getTemplateArgs($settings),
            [Controller::TEMPLATE_KEY__IS_LOADED => '',]
        );

        $this->assertEquals([Controller::TEMPLATE_KEY__IS_LOADED => true,], $actual);
    }

    public function testGetTemplateArgsAdditionalTemplateIsRight()
    {
        $settings   = new Settings();
        $model      = $this->getModel([]);
        $controller = $this->getController($model);

        $actual = array_intersect_key(
            $controller->getTemplateArgs($settings),
            [Controller::TEMPLATE_KEY__TEMPLATE => '',]
        );

        $this->assertEquals(
            [
                Controller::TEMPLATE_KEY__TEMPLATE => $controller::GetPathToTwigTemplate($settings),
            ],
            $actual
        );
    }

    ////

    public function testGetDependencies()
    {
        $controllerA = $this->getController(null);

        $controllerB = new class (null, $controllerA) extends Controller {

            protected $controllerA;

            public function __construct(?Model $model = null, $controllerA)
            {
                parent::__construct($model);

                $this->controllerA = $controllerA;
            }
        };

        $this->assertEquals(
            [
                get_class($controllerA),
            ],
            $controllerB->getDependencies()
        );
    }

    public function testGetDependenciesWithSubDependencies()
    {
        $controllerA = new class extends Controller {

            public function getDependencies(string $sourceClass = ''): array
            {
                return [
                    'A',
                ];
            }
        };

        $controllerB = new class (null, $controllerA) extends Controller {

            protected $controllerA;

            public function __construct(?Model $model = null, $controllerA)
            {
                parent::__construct($model);

                $this->controllerA = $controllerA;
            }
        };

        $this->assertEquals(
            [
                'A',
                get_class($controllerA),
            ],
            $controllerB->getDependencies()
        );
    }

    public function testGetDependenciesWithSubDependenciesRecursively()
    {
        $controllerA = new class extends Controller {

            public function getDependencies(string $sourceClass = ''): array
            {
                return [
                    'A',
                ];
            }
        };

        $controllerB = new class (null, $controllerA) extends Controller {

            protected $controllerA;

            public function __construct(?Model $model = null, $controllerA)
            {
                parent::__construct($model);

                $this->controllerA = $controllerA;
            }
        };

        $controllerC = new class (null, $controllerB) extends Controller {

            protected $controllerB;

            public function __construct(?Model $model = null, $controllerB)
            {
                parent::__construct($model);

                $this->controllerB = $controllerB;
            }
        };

        $this->assertEquals(
            [
                'A',
                get_class($controllerA),
                get_class($controllerB),
            ],
            $controllerC->getDependencies()
        );
    }

    public function testGetDependenciesWithSubDependenciesInOrderWhenSubBeforeMainDependency()
    {
        $controllerA = new class extends Controller {

            public function getDependencies(string $sourceClass = ''): array
            {
                return [
                    'A',
                ];
            }
        };

        $controllerB = new class (null, $controllerA) extends Controller {

            protected $controllerA;

            public function __construct(?Model $model = null, $controllerA)
            {
                parent::__construct($model);

                $this->controllerA = $controllerA;
            }
        };

        $this->assertEquals(
            [
                'A',
                get_class($controllerA),
            ],
            $controllerB->getDependencies()
        );
    }

    public function testGetDependenciesWithSubDependenciesWhenBlocksAreDependentFromEachOther()
    {
        $controllerA = new class extends Controller {

            protected $controllerB;

            public function setControllerB($controllerB)
            {
                $this->controllerB = $controllerB;
            }
        };

        $controllerB = new class (null, $controllerA) extends Controller {

            protected $controllerA;

            public function __construct(?Model $model = null, $controllerA)
            {
                parent::__construct($model);

                $this->controllerA = $controllerA;
            }
        };

        $controllerA->setControllerB($controllerB);

        $this->assertEquals(
            [
                get_class($controllerA),
            ],
            $controllerB->getDependencies()
        );
    }

    public function testGetDependenciesWithoutDuplicatesWhenSeveralWithOneType()
    {
        $controllerA = $this->getController(null);

        $controllerB = new class (null, $controllerA) extends Controller {

            protected $controllerA;
            protected $controllerAA;
            protected $controllerAAA;

            public function __construct(?Model $model = null, $controllerA)
            {
                parent::__construct($model);

                $this->controllerA   = $controllerA;
                $this->controllerAA  = $controllerA;
                $this->controllerAAA = $controllerA;
            }
        };

        $this->assertEquals(
            [
                get_class($controllerA),
            ],
            $controllerB->getDependencies()
        );
    }

    ////

    public function testAutoInitModel()
    {
        $modelClass      = str_replace(['::', '\\'], '_', __METHOD__);
        $controllerClass = $modelClass . Settings::$controllerSuffix;
        eval('class ' . $modelClass . ' extends ' . Model::class . ' {}');
        eval('class ' . $controllerClass . ' extends ' . Controller::class . ' {}');
        $controller = new $controllerClass();

        $actualModelClass = $controller->getModel() ?
            get_class($controller->getModel()) :
            '';

        $this->assertEquals($modelClass, $actualModelClass);
    }

    public function testAutoInitModelWhenModelHasWrongClass()
    {
        $modelClass      = str_replace(['::', '\\'], '_', __METHOD__);
        $controllerClass = $modelClass . Settings::$controllerSuffix;
        eval('class ' . $modelClass . ' {}');
        eval('class ' . $controllerClass . ' extends ' . Controller::class . ' {}');
        $controller = new $controllerClass();

        $this->assertEquals(null, $controller->getModel());
    }
}
