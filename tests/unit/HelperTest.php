<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework\Tests\unit;

use Codeception\Test\Unit;
use LightSource\FrontBlocksFramework\Helper;

class HelperTest extends Unit
{

    public function testArrayFilterWithoutSaveKeys()
    {
        $this->assertEquals(
            [
                0 => '2',
            ],
            Helper::ArrayFilter(
                ['1', '2'],
                function ($value) {
                    return '1' !== $value;
                },
                false
            )
        );
    }

    public function testArrayFilterWithSaveKeys()
    {
        $this->assertEquals(
            [
                1 => '2',
            ],
            Helper::ArrayFilter(
                ['1', '2'],
                function ($value) {
                    return '1' !== $value;
                },
                true
            )
        );
    }

    public function testArrayMergeRecursive()
    {
        $this->assertEquals(
            [
                'classes' => [
                    'first',
                    'second',
                ],
                'value'   => 2,
            ],
            Helper::arrayMergeRecursive(
                [
                    'classes' => [
                        'first',
                    ],
                    'value'   => 1,
                ],
                [
                    'classes' => [
                        'second',
                    ],
                    'value'   => 2,
                ]
            )
        );
    }
}
