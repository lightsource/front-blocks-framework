<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework\Tests\unit;

use Codeception\Test\Unit;
use LightSource\FrontBlocksFramework\BlockDuplicator;
use org\bovigo\vfs\vfsStream;

class BlockDuplicatorTest extends Unit
{

    // get a unique directory name depending on a test method to prevent affect other tests
    private function getUniqueDirName(string $methodConstant): string
    {
        return str_replace([':', '\\'], '_', $methodConstant);
    }

    private function testCopy(
        string $methodConstant,
        array $fileStructure,
        string $sourceControllerFile,
        string $targetControllerFile
    ): string {
        $rootDirName = $this->getUniqueDirName($methodConstant);
        $directory   = vfsStream::setup($rootDirName);
        vfsStream::create($fileStructure, $directory);
        $directoryUrl = $directory->url();

        $blockDuplicator = new BlockDuplicator($directoryUrl, $sourceControllerFile, $targetControllerFile, 'C');
        $blockDuplicator->copy();

        return $directoryUrl;
    }

    public function testCopyFiles()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'ExampleC.php' => '',
                    'example.twig'  => '',
                ],
            ],
            'Example/ExampleC',
            'Test/TestC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'test.twig',
                    ]
                )
            )
        );
    }

    public function testCopyIgnoreFolders()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'ExampleC.php' => '',
                    'EmptyFolder'   => [],
                ],
            ],
            'Example/ExampleC.php',
            'Test/TestC.php'
        );

        $this->assertFalse(
            is_dir(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'EmptyFolder',
                    ]
                )
            )
        );
    }

    public function testCopyIgnoreWhenTargetFolderAlreadyExists()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'ExampleC.php' => '',
                    'example.twig'  => '',
                ],
                'Test'    => [

                ],
            ],
            'Example/ExampleC.php',
            'Test/TestC.php'
        );

        $this->assertFalse(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'example.twig',
                    ]
                )
            )
        );
    }

    public function testCopyWhenTargetContainsSeveralNewFolders()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'ExampleC.php' => '',
                    'example.twig'  => '',
                ],
            ],
            'Example/ExampleC.php',
            'Test/Theme/Main/Test_Theme_MainC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'Theme',
                        'Main',
                        'test--theme--main.twig',
                    ]
                )
            )
        );
    }

    //// replace in resources

    public function testCopyReplaceNameInResources()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'ExampleC.php' => '',
                    'example.twig'  => '',
                ],
            ],
            'Example/ExampleC',
            'Test/TestC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'test.twig',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInResourcesWithTheme()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'Theme' => [
                        'Main' => [
                            'Example_Theme_MainC.php'  => '',
                            'example--theme--main.twig' => '',
                        ],
                    ],
                ],
            ],
            'Example/Theme/Main/Example_Theme_MainC',
            'Test/Theme/New/Test_Theme_NewC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'Theme',
                        'New',
                        'test--theme--new.twig',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInResourcesWithCamelCase()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'ExampleName' => [
                    'ExampleNameC.php' => '',
                    'example-name.twig' => '',
                ],
            ],
            'ExampleName/ExampleNameC.php',
            'TestName/TestNameC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'TestName',
                        'test-name.twig',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInResourcesContent()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'ExampleC.php' => '',
                    'example.twig'  => '<div class="example"></div>',
                ],
            ],
            'Example/ExampleC',
            'Test/TestC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Test',
                'test.twig',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals('<div class="test"></div>', $fileContent);
    }

    public function testCopyReplaceNameInResourcesContentWithTheme()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'Theme' => [
                        'Main' => [
                            'Example_Theme_MainC.php'  => '',
                            'example--theme--main.twig' => '<div class="example--theme--main"></div>',
                        ],
                    ],
                ],
            ],
            'Example/Theme/Main/Example_Theme_MainC',
            'Test/Theme/New/Test_Theme_NewC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Test',
                'Theme',
                'New',
                'test--theme--new.twig',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals('<div class="test--theme--new"></div>', $fileContent);
    }

    public function testCopyReplaceNameInResourcesContentWithCamelCase()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'ExampleName' => [
                    'ExampleNameC.php' => '',
                    'example-name.twig' => '<div class="example-name"></div>',
                ],
            ],
            'ExampleName/ExampleNameC',
            'TestName/TestNameC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'TestName',
                'test-name.twig',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals('<div class="test-name"></div>', $fileContent);
    }

    //// replace in controller and theme

    public function testCopyReplaceNameInController()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'ExampleC.php' => '',
                ],
            ],
            'Example/ExampleC',
            'Test/TestC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'TestC.php',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInControllerWithTheme()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'Theme' => [
                        'Main' => [
                            'Example_Theme_MainC.php' => '',
                        ],
                    ],
                ],
            ],
            'Example/Theme/Main/Example_Theme_MainC',
            'Test/Theme/New/Test_Theme_NewC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'Theme',
                        'New',
                        'Test_Theme_NewC.php',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInControllerWithCamelCase()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'ExampleName' => [
                    'ExampleNameC.php' => '',
                ],
            ],
            'ExampleName/ExampleNameC',
            'TestName/TestNameC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'TestName',
                        'TestNameC.php',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInModel()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'Example.php'   => '',
                    'ExampleC.php' => '',
                ],
            ],
            'Example/ExampleC',
            'Test/TestC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'Test.php',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInModelWithTheme()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'Theme' => [
                        'Main' => [
                            'Example_Theme_Main.php'   => '',
                            'Example_Theme_MainC.php' => '',
                        ],
                    ],
                ],
            ],
            'Example/Theme/Main/Example_Theme_MainC',
            'Test/Theme/New/Test_Theme_NewC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'Test',
                        'Theme',
                        'New',
                        'Test_Theme_New.php',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInModelWithCamelCase()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'ExampleName' => [
                    'ExampleName.php'   => '',
                    'ExampleNameC.php' => '',
                ],
            ],
            'ExampleName/ExampleNameC',
            'TestName/TestNameC.php'
        );

        $this->assertTrue(
            is_file(
                implode(
                    DIRECTORY_SEPARATOR,
                    [
                        $directoryUrl,
                        'TestName',
                        'TestName.php',
                    ]
                )
            )
        );
    }

    public function testCopyReplaceNameInControllerContent()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'ExampleC.php' => '<?php namespace Blocks\Example; 
use LightSource\FrontBlocksFramework\Controller; 
class ExampleC extends Controller{}',
                ],
            ],
            'Example/ExampleC',
            'Test/TestC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Test',
                'TestC.php',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals(
            '<?php namespace Blocks\Test; 
use LightSource\FrontBlocksFramework\Controller; 
class TestC extends Controller{}',
            $fileContent
        );
    }

    public function testCopyReplaceNameInControllerContentWithTheme()
    {
        $controllerClass = '<?php namespace Blocks\Example\Theme\Main; 
use LightSource\FrontBlocksFramework\Controller; 
class Example_Theme_MainC extends Controller{}';

        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'Theme' => [
                        'Main' => [
                            'Example_Theme_MainC.php' => $controllerClass,
                        ],
                    ],
                ],
            ],
            'Example/Theme/Main/Example_Theme_MainC',
            'Test/Theme/Neww/Test_Theme_NewwC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Test',
                'Theme',
                'Neww',
                'Test_Theme_NewwC.php',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals(
            '<?php namespace Blocks\Test\Theme\Neww; 
use LightSource\FrontBlocksFramework\Controller; 
class Test_Theme_NewwC extends Controller{}',
            $fileContent
        );
    }

    public function testCopyReplaceNameInControllerContentWithCamelCase()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'ExampleName' => [
                    'ExampleNameC.php' => '<?php namespace Blocks\ExampleName; 
use LightSource\FrontBlocksFramework\Controller; 
class ExampleNameC extends Controller{}',
                ],
            ],
            'ExampleName/ExampleNameC',
            'TestName/TestNameC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'TestName',
                'TestNameC.php',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals(
            '<?php namespace Blocks\TestName; 
use LightSource\FrontBlocksFramework\Controller; 
class TestNameC extends Controller{}',
            $fileContent
        );
    }

    public function testCopyReplaceNameInModelContent()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'Example.php'   => '<?php namespace Blocks\Example; 
use LightSource\FrontBlocksFramework\Controller; 
class Example extends Controller{}',
                    'ExampleC.php' => '',
                ],
            ],
            'Example/ExampleC',
            'Test/TestC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Test',
                'Test.php',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals(
            '<?php namespace Blocks\Test; 
use LightSource\FrontBlocksFramework\Controller; 
class Test extends Controller{}',
            $fileContent
        );
    }

    public function testCopyReplaceNameInModelContentWithTheme()
    {
        $modelClass = '<?php namespace Blocks\Example\Theme\Main; 
use LightSource\FrontBlocksFramework\Controller; 
class Example_Theme_Main extends Controller{}';

        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'Example' => [
                    'Theme' => [
                        'Main' => [
                            'Example_Theme_Main.php'   => $modelClass,
                            'Example_Theme_MainC.php' => '',
                        ],
                    ],
                ],
            ],
            'Example/Theme/Main/Example_Theme_MainC',
            'Test/Theme/Neww/Test_Theme_NewwC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'Test',
                'Theme',
                'Neww',
                'Test_Theme_Neww.php',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals(
            '<?php namespace Blocks\Test\Theme\Neww; 
use LightSource\FrontBlocksFramework\Controller; 
class Test_Theme_Neww extends Controller{}',
            $fileContent
        );
    }

    public function testCopyReplaceNameInModelContentWithCamelCase()
    {
        $directoryUrl = $this->testCopy(
            __METHOD__,
            [
                'ExampleName' => [
                    'ExampleName.php'   => '<?php namespace Blocks\ExampleName; 
use LightSource\FrontBlocksFramework\Controller; 
class ExampleName extends Controller{}',
                    'ExampleNameC.php' => '',
                ],
            ],
            'ExampleName/ExampleNameC',
            'TestName/TestNameC.php'
        );

        $fileName    = implode(
            DIRECTORY_SEPARATOR,
            [
                $directoryUrl,
                'TestName',
                'TestName.php',
            ]
        );
        $fileContent = is_file($fileName) ?
            file_get_contents($fileName) :
            '';

        $this->assertEquals(
            '<?php namespace Blocks\TestName; 
use LightSource\FrontBlocksFramework\Controller; 
class TestName extends Controller{}',
            $fileContent
        );
    }
}
