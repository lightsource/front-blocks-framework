<?php

declare(strict_types=1);

namespace LightSource\FrontBlocksFramework\Tests\unit;

use Codeception\Test\Unit;
use LightSource\FrontBlocksFramework\Controller;
use LightSource\FrontBlocksFramework\FieldsReader;
use LightSource\FrontBlocksFramework\Model;

class FieldsReaderTest extends Unit
{

    public function testReadProtectedField()
    {
        $fieldsReader = new class extends FieldsReader {

            protected $loadedField;

            public function __construct()
            {
                parent::__construct();
            }

            public function getFields()
            {
                return $this->getFieldsInfo();
            }
        };

        $this->assertEquals(
            [
                'loadedField' => '',
            ],
            $fieldsReader->getFields()
        );
    }

    public function testIgnoreReadPublicField()
    {
        $fieldsReader = new class extends FieldsReader {

            public $unloadedField;

            public function __construct()
            {
                parent::__construct();
            }

            public function getFields()
            {
                return $this->getFieldsInfo();
            }
        };

        $this->assertEquals(
            [
            ],
            $fieldsReader->getFields()
        );
    }

    public function testIgnoreReadPrivateField()
    {
        $fieldsReader = new class extends FieldsReader {

            private $unloadedField;

            public function __construct()
            {
                parent::__construct();
            }

            public function getFields()
            {
                return $this->getFieldsInfo();
            }
        };

        $this->assertEquals(
            [
            ],
            $fieldsReader->getFields()
        );
    }

    public function testReadFieldWithType()
    {
        $fieldsReader = new class extends FieldsReader {

            protected string $loadedField;

            public function __construct()
            {
                parent::__construct();
            }

            public function getFields()
            {
                return $this->getFieldsInfo();
            }
        };

        $this->assertEquals(
            [
                'loadedField' => 'string',
            ],
            $fieldsReader->getFields()
        );
    }

    public function testReadFieldWithoutType()
    {
        $fieldsReader = new class extends FieldsReader {

            protected $loadedField;

            public function __construct()
            {
                parent::__construct();
            }

            public function getFields()
            {
                return $this->getFieldsInfo();
            }
        };

        $this->assertEquals(
            [
                'loadedField' => '',
            ],
            $fieldsReader->getFields()
        );
    }

    ////

    public function testAutoInitIntField()
    {
        $fieldsReader = new class extends FieldsReader {

            protected int $int;

            public function __construct()
            {
                parent::__construct();
            }

            public function getInt()
            {
                return $this->int;
            }
        };

        $this->assertTrue(0 === $fieldsReader->getInt());
    }

    public function testAutoInitFloatField()
    {
        $fieldsReader = new class extends FieldsReader {

            protected float $float;

            public function __construct()
            {
                parent::__construct();
            }

            public function getFloat()
            {
                return $this->float;
            }
        };

        $this->assertTrue(0.0 === $fieldsReader->getFloat());
    }

    public function testAutoInitStringField()
    {
        $fieldsReader = new class extends FieldsReader {

            protected string $string;

            public function __construct()
            {
                parent::__construct();
            }

            public function getString()
            {
                return $this->string;
            }
        };

        $this->assertTrue('' === $fieldsReader->getString());
    }

    public function testAutoInitBoolField()
    {
        $fieldsReader = new class extends FieldsReader {

            protected bool $bool;

            public function __construct()
            {
                parent::__construct();
            }

            public function getBool()
            {
                return $this->bool;
            }
        };

        $this->assertTrue(false === $fieldsReader->getBool());
    }

    public function testAutoInitArrayField()
    {
        $fieldsReader = new class extends FieldsReader {

            protected array $array;

            public function __construct()
            {
                parent::__construct();
            }

            public function getArray()
            {
                return $this->array;
            }
        };

        $this->assertTrue([] === $fieldsReader->getArray());
    }

    public function testAutoInitModelField()
    {
        $testModel        = new class extends Model {
        };
        $testModelClass   = get_class($testModel);
        $fieldsReader     = new class ($testModelClass) extends FieldsReader {

            protected $model;
            private $testClass;

            public function __construct($testClass)
            {
                $this->testClass = $testClass;
                parent::__construct();
            }

            public function getFieldType(string $fieldName): ?string
            {
                return ('model' === $fieldName ?
                    $this->testClass :
                    parent::getFieldType($fieldName));
            }

            public function getModel()
            {
                return $this->model;
            }
        };
        $actualModelClass = $fieldsReader->getModel() ?
            get_class($fieldsReader->getModel()) :
            '';

        $this->assertEquals($actualModelClass, $testModelClass);
    }

    public function testAutoInitControllerField()
    {
        $testController      = new class extends Controller {
        };
        $testControllerClass = get_class($testController);
        $fieldsReader        = new class ($testControllerClass) extends FieldsReader {

            protected $controller;
            private $testClass;

            public function __construct($testControllerClass)
            {
                $this->testClass = $testControllerClass;
                parent::__construct();
            }

            public function getFieldType(string $fieldName): ?string
            {
                return ('controller' === $fieldName ?
                    $this->testClass :
                    parent::getFieldType($fieldName));
            }

            public function getController()
            {
                return $this->controller;
            }
        };
        $actualModelClass    = $fieldsReader->getController() ?
            get_class($fieldsReader->getController()) :
            '';

        $this->assertEquals($actualModelClass, $testControllerClass);
    }

    public function testIgnoreInitFieldWithoutType()
    {
        $fieldsReader = new class extends FieldsReader {

            protected $default;

            public function __construct()
            {
                parent::__construct();
            }

            public function getDefault()
            {
                return $this->default;
            }
        };

        $this->assertTrue(null === $fieldsReader->getDefault());
    }
}
